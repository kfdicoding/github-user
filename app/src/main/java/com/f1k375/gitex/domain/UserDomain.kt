package com.f1k375.gitex.domain

import android.os.Parcelable
import com.f1k375.gitex.data.local.UserEntity
import com.f1k375.gitex.data.models.UserResponse
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserDomain(
    val id: Int,
    val name: String,
    val loginName: String,
    val avatarUrl: String,
    val location: String,
    val bio: String,
    val htmlUrl: String,
    val company: String,
    val publicRepos: Int,
    val email: String,
    val followers: Int,
    val following: Int,
    val updatedAt: String,
    val createdAt: String,
): Parcelable

val UserResponse.asDomain
get() = UserDomain(
    id = id ?: 0,
    name = name ?: "",
    loginName = login ?: "",
    avatarUrl = avatarUrl ?: "",
    location = location ?: "",
    bio = bio ?: "",
    htmlUrl = htmlUrl ?: "",
    company = company ?: "",
    publicRepos = publicRepos ?: 0,
    email = email ?: "",
    followers = followers ?: 0,
    following = following ?: 0,
    updatedAt = updatedAt ?: "",
    createdAt = createdAt ?: ""
)

val UserEntity.asDomain
get() = UserDomain(
    id = id,
    name = loginName,
    loginName = loginName,
    avatarUrl = avatar,
    location = "",
    bio = "",
    htmlUrl = htmlUrl,
    company = "",
    publicRepos = 0,
    email = "",
    followers = 0,
    following = 0,
    updatedAt = "",
    createdAt = ""
)
