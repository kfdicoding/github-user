package com.f1k375.gitex

import android.app.Application
import com.f1k375.gitex.helpers.ModuleApp
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class GitexApp: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(applicationContext)
            modules(ModuleApp.listModules)
        }
    }


}
