package com.f1k375.gitex.helpers

sealed class ViewState<out T: Any> {
    data class DataLoaded<out T: Any>(val data: T) : ViewState<T>()
    object Loading : ViewState<Nothing>()
    data class Error(val message: String) : ViewState<Nothing>()
}
