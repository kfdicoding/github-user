package com.f1k375.gitex.helpers.extension


fun Boolean.doThis(job: () -> Unit) {
    if (this)
        job()
}

fun Boolean.alsoDoThis(job: () -> Unit): Boolean {
    if (this)
        job()
    return this
}

fun Boolean.notDoThis(job: () -> Unit) {
    if (!this)
        job()
}

fun Boolean.alsoNotDoThis(job: () -> Unit): Boolean {
    if (!this)
        job()
    return this
}
