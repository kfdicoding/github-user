package com.f1k375.gitex.helpers.preferences

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class SettingPreference(
    private val dataStore: DataStore<Preferences>
) {

    private val THEME_KEY = booleanPreferencesKey("theme_setting")

    val isDarkMode: Flow<Boolean>
    get() = dataStore.data.map { preferences->
        preferences[THEME_KEY] ?: false
    }

    suspend fun setDarkMode(isDarkMode: Boolean) =  dataStore.edit{preferences->
        preferences[THEME_KEY] = isDarkMode
    }

}
