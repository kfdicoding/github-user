package com.f1k375.gitex.helpers.extension

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Looper
import android.os.Parcelable
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*

internal fun checkMainThread() {
    check(Looper.myLooper() == Looper.getMainLooper()) {
        "Expected to be called on the main thread but was " + Thread.currentThread().name
    }
}

inline fun <reified T : Parcelable> Intent.parcelable(key: String): T? = when {
    Build.VERSION.SDK_INT > 33 -> getParcelableExtra(key, T::class.java)
    else -> @Suppress("DEPRECATION") getParcelableExtra(key) as? T
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

fun Activity.showToast(message: String, isLongDisplay: Boolean = false) {
    Toast.makeText(
        applicationContext, message,
        if (isLongDisplay)
            Toast.LENGTH_LONG
        else
            Toast.LENGTH_SHORT
    ).show()
}

fun Fragment.showToast(message: String, isLongDisplay: Boolean = false) {
    Toast.makeText(
        requireContext(), message,
        if (isLongDisplay)
            Toast.LENGTH_LONG
        else
            Toast.LENGTH_SHORT
    ).show()
}

@FlowPreview
@ExperimentalCoroutinesApi
fun SearchView.setOnQueryTextListenerFlow(
    scope: CoroutineScope,
    activity: Activity,
    actionQueryChange: (key: String) -> Unit
) {
    callbackFlow {
        checkMainThread()
        setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(key: String?): Boolean {
                activity.hideKeyboard()
                trySend(key ?: "")
                return false
            }

            override fun onQueryTextChange(key: String?): Boolean {
                trySend(key ?: "")
                return false
            }

        })
        awaitClose { setOnQueryTextListener(null) }
    }.filterNotNull()
        .debounce(600)
        .onEach { actionQueryChange(it) }
        .launchIn(scope)
}
