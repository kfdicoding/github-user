package com.f1k375.gitex.helpers

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.room.Room
import com.f1k375.gitex.data.local.LocalClient
import com.f1k375.gitex.data.remote.Network
import com.f1k375.gitex.data.remote.UserPersistence
import com.f1k375.gitex.data.remote.UserPersistenceImpl
import com.f1k375.gitex.data.remote.UserService
import com.f1k375.gitex.data.repositories.UserRepository
import com.f1k375.gitex.data.repositories.UserRepositoryImpl
import com.f1k375.gitex.helpers.preferences.SettingPreference
import com.f1k375.gitex.ui.viewmodels.DetailUserViewModel
import com.f1k375.gitex.ui.viewmodels.SearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object ModuleApp {

    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")

    private fun networkModule() = module {
        single { Network.getOkhttp() }
        single { Network.getRetrofit(get(), Network.BASE_URL) }
        single { get<Context>().dataStore }
        single { SettingPreference(get()) }

    }

    private fun localClientModule() = module {
        single {
            Room.databaseBuilder(
                get(),
                LocalClient::class.java,
                "gitex_app.db"
            ).fallbackToDestructiveMigration().build()
        }
    }

    private fun persistenceModule() = module {
        single { Network.provideRetrofitService(get(), UserService::class.java) }
        single<UserPersistence> { UserPersistenceImpl(get()) }
    }

    private fun repositoryModule() = module {
        factory<UserRepository> { UserRepositoryImpl(get(), get()) }
    }

    private fun viewModelModule() = module {
        viewModel { SearchViewModel(get()) }
        viewModel { DetailUserViewModel(get()) }
    }

    val listModules
    get() = listOf(
        networkModule(),
        localClientModule(),
        persistenceModule(),
        repositoryModule(),
        viewModelModule()
    )
}
