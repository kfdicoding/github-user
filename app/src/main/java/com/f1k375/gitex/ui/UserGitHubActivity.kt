package com.f1k375.gitex.ui

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.f1k375.gitex.R
import com.f1k375.gitex.databinding.ActivityUserGitHubBinding
import com.f1k375.gitex.helpers.ViewState
import com.f1k375.gitex.helpers.extension.alsoDoThis
import com.f1k375.gitex.helpers.extension.doThis
import com.f1k375.gitex.helpers.extension.setOnQueryTextListenerFlow
import com.f1k375.gitex.helpers.extension.viewBinding
import com.f1k375.gitex.helpers.preferences.SettingPreference
import com.f1k375.gitex.ui.adapter.AdapterUserList
import com.f1k375.gitex.ui.favoriteuser.FavoriteActivity
import com.f1k375.gitex.ui.viewmodels.SearchViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.koin.android.ext.android.inject

class UserGitHubActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityUserGitHubBinding::inflate)
    private val viewModel: SearchViewModel by inject()
    private val settings: SettingPreference by inject()

    private val adapterListUser by lazy { AdapterUserList() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setView()
        setObserver()

        checkDefaultSettingTheme()

        binding.searchView.setQuery(viewModel.searchKey, true)
    }

    private fun setView(){
        setSearchView()
        setAdapter()

        binding.buttonSwitchModeDayNight.setOnClickListener {
            lifecycleScope.launchWhenResumed {
                settings.setDarkMode(!isDarkMode)
            }
        }
        binding.buttonOpenFavorite.setOnClickListener {
            startActivity(Intent(this, FavoriteActivity::class.java))
        }
    }

    @OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
    private fun setSearchView(){
        binding.searchView.setOnQueryTextListenerFlow(lifecycleScope, this){key->
            viewModel.performSearch(key)
        }
    }

    private fun setAdapter(){
        binding.rvUser.apply {
            setHasFixedSize(true)
            adapter = adapterListUser
        }
    }

    private fun setObserver(){
        searchObserver()
        settingObserver()
    }

    private fun searchObserver(){
        viewModel.userSearch.observe(this){result->
            when(result){
                is ViewState.Loading -> {
                    setStateDataFlow(isLoading = true)
                }
                is ViewState.Error -> {
                    setStateDataFlow(isError = true, message = result.message)
                }
                is ViewState.DataLoaded -> {
                    adapterListUser.submitData(result.data)
                    setStateDataFlow(isEmpty = result.data.isEmpty())
                }
            }
        }
    }

    private var isDarkMode = false
    private fun settingObserver(){
        lifecycleScope.launchWhenResumed {
            settings.isDarkMode.collect {isDark->
                isDarkMode = isDark
                setThemeSetting()
            }
        }
    }

    private fun setThemeSetting(){
        binding.buttonSwitchModeDayNight.setImageResource(
            if (isDarkMode) R.drawable.ic_sunny
            else R.drawable.ic_mode_night
        )
        AppCompatDelegate.setDefaultNightMode(if(isDarkMode) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO)
    }

    private fun checkDefaultSettingTheme(){
        val nightModeFlags: Int = resources.configuration.uiMode and
                Configuration.UI_MODE_NIGHT_MASK
        val isDark = when (nightModeFlags) {
            Configuration.UI_MODE_NIGHT_YES -> true
            Configuration.UI_MODE_NIGHT_NO -> false
            Configuration.UI_MODE_NIGHT_UNDEFINED -> false
            else -> false
        }

        (isDarkMode != isDark).doThis {
            lifecycleScope.launchWhenResumed {
                settings.setDarkMode(isDark)
            }
        }
    }

    private fun setStateDataFlow(
        isLoading: Boolean = false,
        isError: Boolean = false,
        isEmpty: Boolean = false,
        message: String = ""
    ){
        binding.run {
            rvUser.isVisible = isLoading.not() && isError.not()
            layoutLoading.root.isVisible = isLoading
            layoutError.root.isVisible = isError.alsoDoThis {
                layoutError.tvError.text = getString(R.string.error_info, message)
            }
            layoutEmpty.root.isVisible = isEmpty
        }
    }
}
