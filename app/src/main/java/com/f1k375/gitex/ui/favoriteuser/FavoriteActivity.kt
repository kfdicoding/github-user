package com.f1k375.gitex.ui.favoriteuser

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.f1k375.gitex.data.repositories.UserRepository
import com.f1k375.gitex.databinding.ActivityFavoriteBinding
import com.f1k375.gitex.helpers.extension.showToast
import com.f1k375.gitex.helpers.extension.viewBinding
import com.f1k375.gitex.ui.adapter.AdapterUserList
import org.koin.android.ext.android.inject

class FavoriteActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityFavoriteBinding::inflate)
    private val repository: UserRepository by inject()

    private val adapterListUser by lazy { AdapterUserList() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setAdapter()
        setObserver()
    }

    private fun setAdapter(){
        binding.rvUser.apply {
            setHasFixedSize(true)
            adapter = adapterListUser
        }
    }

    private fun setObserver(){
        lifecycleScope.launchWhenResumed {
            repository.favoriteUser().collect{result->
                result.onSuccess {
                    adapterListUser.submitData(it)
                }.onFailure {
                    showToast(it.localizedMessage ?: "Something crash")
                }
            }
        }
    }
}
