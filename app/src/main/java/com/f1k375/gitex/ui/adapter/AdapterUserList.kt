package com.f1k375.gitex.ui.adapter

import android.content.Intent
import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.f1k375.gitex.R
import com.f1k375.gitex.databinding.ItemUserListBinding
import com.f1k375.gitex.domain.UserDomain
import com.f1k375.gitex.helpers.extension.alsoDoThis
import com.f1k375.gitex.helpers.extension.alsoNotDoThis
import com.f1k375.gitex.ui.detailuser.DetailUserActivity

class AdapterUserList: Adapter<AdapterUserList.ViewHolderUserList>(){

    inner class ViewHolderUserList(private val mBinding: ItemUserListBinding): ViewHolder(mBinding.root){

        fun bind(position: Int, item: UserDomain){
            val isLeftShow = position%2==0
            mBinding.run {
                userAvatarLeft.isInvisible = isLeftShow.not().alsoNotDoThis {
                    Glide.with(root.context).load(item.avatarUrl).into(userAvatarLeft)
                }
                userAvatarRight.isVisible = isLeftShow.not().alsoDoThis {
                    Glide.with(root.context).load(item.avatarUrl).into(userAvatarRight)
                }

                userNameLeft.text = item.name.ifEmpty { item.loginName }
                userNameRight.text = userNameLeft.text

                userUsernameLeft.text = root.context.getString(R.string.username, item.loginName)
                userUsernameRight.text = userUsernameLeft.text

                viewLeft.isInvisible = isLeftShow.not()
                viewRight.isVisible = isLeftShow.not()

                userUrlPage.text = item.htmlUrl
                userUrlPage.gravity = if (isLeftShow) Gravity.START else Gravity.END

                root.setOnClickListener {
                    root.context.startActivity(
                        Intent(root.context, DetailUserActivity::class.java).apply {
                            putExtra(DetailUserActivity.USER_DATA, item)
                        }
                    )
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderUserList {
        return ViewHolderUserList(
            ItemUserListBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolderUserList, position: Int) {
        holder.bind(position, data[position])
    }

    override fun getItemCount(): Int {
        return data.size
    }

    private val data = ArrayList<UserDomain>()

    fun submitData(newData: List<UserDomain>){
        val diffCallback = UserDiffUtil(data, newData)
        val resultDiff = DiffUtil.calculateDiff(diffCallback)
        data.clear()
        data.addAll(newData)
        resultDiff.dispatchUpdatesTo(this)
    }
}
