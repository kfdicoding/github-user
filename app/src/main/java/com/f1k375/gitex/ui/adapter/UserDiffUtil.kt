package com.f1k375.gitex.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import com.f1k375.gitex.domain.UserDomain

class UserDiffUtil(
    private val old: List<UserDomain>,
    private val new: List<UserDomain>
): DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return old.size
    }

    override fun getNewListSize(): Int {
        return new.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return old[oldItemPosition] == new[newItemPosition]
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return old[oldItemPosition].loginName == new[newItemPosition].loginName
    }
}
