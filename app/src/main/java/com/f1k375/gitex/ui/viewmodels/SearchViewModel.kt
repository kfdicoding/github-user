package com.f1k375.gitex.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.f1k375.gitex.data.repositories.UserRepository
import com.f1k375.gitex.domain.UserDomain
import com.f1k375.gitex.helpers.ViewState
import kotlinx.coroutines.launch

class SearchViewModel(
    private val repository: UserRepository
): ViewModel(){

    var searchKey = "\"\""

    fun performSearch(key: String){
        searchKey = key
        searchUser()
    }

    private val _userSearch = MutableLiveData<ViewState<List<UserDomain>>>()
    val userSearch: LiveData<ViewState<List<UserDomain>>>
    get() = _userSearch

    private fun searchUser(){
        viewModelScope.launch {
            _userSearch.postValue(ViewState.Loading)
            repository.searchUser(searchKey).onSuccess {
                _userSearch.postValue(ViewState.DataLoaded(it))
            }.onFailure {
                _userSearch.postValue(ViewState.Error(it.message ?: it.localizedMessage))
            }
        }
    }


}
