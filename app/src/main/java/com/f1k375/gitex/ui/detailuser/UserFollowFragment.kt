package com.f1k375.gitex.ui.detailuser

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.f1k375.gitex.R
import com.f1k375.gitex.databinding.FragmentUserFollowBinding
import com.f1k375.gitex.helpers.ViewState
import com.f1k375.gitex.helpers.consume
import com.f1k375.gitex.helpers.extension.alsoDoThis
import com.f1k375.gitex.helpers.extension.viewBinding
import com.f1k375.gitex.ui.adapter.AdapterUserList
import com.f1k375.gitex.ui.viewmodels.DetailUserViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class UserFollowFragment: Fragment(R.layout.fragment_user_follow) {

    private val binding by viewBinding(FragmentUserFollowBinding::bind)
    private val viewModel: DetailUserViewModel by sharedViewModel()

    private val adapterListUser by lazy { AdapterUserList() }

    private var screenType = SCREEN_FOLLOWERS

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        screenType = if (savedInstanceState != null) savedInstanceState.getString(SCREEN_TYPE) ?: SCREEN_FOLLOWERS
        else arguments?.getString(SCREEN_TYPE) ?: SCREEN_FOLLOWERS
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.apply {
            putString(SCREEN_TYPE, screenType)
        }
        super.onSaveInstanceState(outState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initSetUpScreen()
    }

    private fun initSetUpScreen(){
        setUpAdapter()

        if (screenType == SCREEN_FOLLOWERS){
            setFollowersScreen()
        }else{
            setFollowingScreen()
        }
    }

    private fun setUpAdapter(){
        binding.rvFollow.setHasFixedSize(true)
        binding.rvFollow.adapter = adapterListUser
    }

    private fun setFollowersScreen(){
        viewModel.getInfoFollowers()
        viewModel.userFollowers.observe(viewLifecycleOwner){it.consume { result->
            when(result){
                is ViewState.Loading -> {
                    setStateDataFlow(isLoading = true)
                }
                is ViewState.Error -> {
                    setStateDataFlow(isEmpty = true, message = result.message)
                }
                is ViewState.DataLoaded -> {
                    adapterListUser.submitData(result.data)
                    setStateDataFlow(isEmpty = result.data.isEmpty())
                }
            }
        }}
    }

    private fun setFollowingScreen(){
        viewModel.getInfoFollowing()
        viewModel.userFollowing.observe(viewLifecycleOwner){it.consume { result->
            when(result){
                is ViewState.Loading -> {
                    setStateDataFlow(isLoading = true)
                }
                is ViewState.Error -> {
                    setStateDataFlow(isEmpty = true, message = result.message)
                }
                is ViewState.DataLoaded -> {
                    adapterListUser.submitData(result.data)
                    setStateDataFlow(isEmpty = result.data.isEmpty())
                }
            }
        }}
    }

    private fun setStateDataFlow(
        isLoading: Boolean = false,
        isError: Boolean = false,
        isEmpty: Boolean = false,
        message: String = ""
    ){
        binding.run {
            rvFollow.isVisible = isLoading.not() && isError.not()
            layoutLoading.root.isVisible = isLoading
            layoutError.root.isVisible = isError.alsoDoThis {
                layoutError.tvError.text = getString(R.string.error_info, message)
            }
            layoutEmpty.root.isVisible = isEmpty
        }
    }

    companion object{
        private const val SCREEN_FOLLOWING = "typeScreenFollowing"
        private const val SCREEN_FOLLOWERS = "typeScreenFollowers"
        private const val SCREEN_TYPE = "whichScreenToShow"

        fun instanceOfScreenFollowing() = createInstance(SCREEN_FOLLOWING)
        fun instanceOfScreenFollowers() = createInstance(SCREEN_FOLLOWERS)

        private fun createInstance(type: String): UserFollowFragment {
            val fragment = UserFollowFragment()
            val args = Bundle()

            args.putString(SCREEN_TYPE, type)
            fragment.arguments = args

            return fragment
        }

    }

}
