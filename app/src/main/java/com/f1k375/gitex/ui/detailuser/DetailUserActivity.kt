package com.f1k375.gitex.ui.detailuser

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.bumptech.glide.Glide
import com.f1k375.gitex.R
import com.f1k375.gitex.databinding.ActivityDetailUserBinding
import com.f1k375.gitex.domain.UserDomain
import com.f1k375.gitex.helpers.ViewState
import com.f1k375.gitex.helpers.dialog.LoadingDialogHelper
import com.f1k375.gitex.helpers.extension.parcelable
import com.f1k375.gitex.helpers.extension.showToast
import com.f1k375.gitex.helpers.extension.viewBinding
import com.f1k375.gitex.ui.viewmodels.DetailUserViewModel
import com.google.android.material.tabs.TabLayoutMediator
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailUserActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityDetailUserBinding::inflate)
    private val viewModel: DetailUserViewModel by viewModel()

    private val loadingHelper by lazy { LoadingDialogHelper(this) }

    private var userDetail: UserDomain? = null

    private var isFavorite: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        userDetail = intent.parcelable(USER_DATA)
        viewModel.username = userDetail?.loginName ?: ""

        viewModel.getDetailUser()
        setView()
        setObserver()
    }

    private fun setView(){
        setToolbar()
        setDetailUser()
        setFollowSection()
    }

    private fun setObserver(){
        detailUserObserver()
        userFavoriteObserver()
    }

    private fun detailUserObserver(){
        viewModel.userDetail.observe(this){result->
            when(result){
                is ViewState.Loading -> loadingHelper.showDialog()
                is ViewState.Error -> {
                    loadingHelper.hideDialog()
                    showToast("error : ${result.message}")
                }
                is ViewState.DataLoaded -> {
                    loadingHelper.hideDialog()
                    userDetail = result.data
                    setDetailUser()
                }
            }
        }
    }

    private fun userFavoriteObserver(){
        lifecycleScope.launchWhenResumed {
            viewModel.isUserFavorite(userDetail?.id ?: 0).observe(this@DetailUserActivity){result->
                result.onSuccess {favorite->
                    isFavorite = favorite
                    setIconFavorite()
                }.onFailure {
                    showToast(it.localizedMessage ?: "Crash on system")
                }
            }
        }
    }

    private fun setIconFavorite(){
        menuDetail?.getItem(0)?.icon = ContextCompat.getDrawable(this@DetailUserActivity, if (isFavorite) R.drawable.ic_favorite_fill else R.drawable.ic_favorite_border)
    }

    private fun setToolbar(){
        setSupportActionBar(binding.toolbar)
        binding.toolbar.setNavigationOnClickListener {
            onBackPressedDispatcher.onBackPressed()
        }
    }

    private fun setDetailUser(){
        userDetail?.let {data->
            binding.run {
                Glide.with(this@DetailUserActivity).load(data.avatarUrl).into(userAvatar)
                userName.text = data.name
                userUsername.text = getString(R.string.username, data.loginName)
                userFollower.text = getString(R.string.follower, data.followers)
                userFollowing.text = getString(R.string.following, data.following)
                userRepositories.text = resources.getQuantityString(R.plurals.repositories, data.publicRepos, data.publicRepos)
                userAddress.text = getString(R.string.address, data.location)
                userCompany.text = getString(R.string.company, data.company)
            }
        }
    }

    private fun doShareDetail(){
        val shareMessage = """
           GitHub User

           User @${userDetail?.loginName} dengan nama ${userDetail?.name}
           dia tinggal di ${userDetail?.location} dan sedang bekerja di ${userDetail?.company}
        """.trimIndent()
        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Sekolah Desain")
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
            startActivity(Intent.createChooser(shareIntent, "choose one"))
        } catch (e: Exception) {
            Toast.makeText(this, "Terjadi kesalahan saat membagikan", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setFollowSection(){
        binding.run {
            viewPageUserFollow.adapter = FollowAdapterPager()
            viewPageUserFollow.offscreenPageLimit = 1
            viewPageUserFollow.isUserInputEnabled = false

            TabLayoutMediator(tabFollowUser, viewPageUserFollow){tab, position->
                if (position == 0) tab.text = "Followers"
                else tab.text = "Following"
            }.attach()
        }
    }

    private var menuDetail: Menu? = null
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_user_menu, menu)
        menuDetail = menu
        setIconFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.menu_share -> {
                doShareDetail()
                true
            }
            R.id.menu_favorite -> {
                userDetail?.let {
                    if (isFavorite) viewModel.removeUserFromFavorite(it.id)
                    else viewModel.addUserToFavorite(it)
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    inner class FollowAdapterPager: FragmentStateAdapter(supportFragmentManager, lifecycle){
        override fun getItemCount(): Int {
            return 2
        }

        override fun createFragment(position: Int): Fragment {
            return if (position == 0) UserFollowFragment.instanceOfScreenFollowers()
            else UserFollowFragment.instanceOfScreenFollowing()
        }


    }

    companion object{
        const val USER_DATA = "user_github_data"
    }
}
