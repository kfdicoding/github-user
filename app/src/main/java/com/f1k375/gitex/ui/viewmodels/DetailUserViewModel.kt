package com.f1k375.gitex.ui.viewmodels

import androidx.lifecycle.*
import com.f1k375.gitex.data.repositories.UserRepository
import com.f1k375.gitex.domain.UserDomain
import com.f1k375.gitex.helpers.OneTimeEvent
import com.f1k375.gitex.helpers.ViewState
import com.f1k375.gitex.helpers.toOneTimeEvent
import kotlinx.coroutines.launch
import kotlin.coroutines.coroutineContext

class DetailUserViewModel(
    private val repository: UserRepository
): ViewModel() {

    var username: String = ""

    private val _userDetail = MutableLiveData<ViewState<UserDomain>>()
    val userDetail: LiveData<ViewState<UserDomain>>
    get() = _userDetail

    fun getDetailUser(){
        viewModelScope.launch {
            _userDetail.postValue(ViewState.Loading)
            repository.detailUser(username).onSuccess {
                _userDetail.postValue(ViewState.DataLoaded(it))
            }.onFailure {
                _userDetail.postValue(ViewState.Error(it.message ?: it.localizedMessage))
            }
        }
    }

    private val _userFollowers = MutableLiveData<OneTimeEvent<ViewState<List<UserDomain>>>>()
    val userFollowers: LiveData<OneTimeEvent<ViewState<List<UserDomain>>>>
    get() = _userFollowers

    fun getInfoFollowers(){
        viewModelScope.launch {
            _userFollowers.postValue(ViewState.Loading.toOneTimeEvent())
            repository.userFollowers(username).onSuccess {
                _userFollowers.postValue(ViewState.DataLoaded(it).toOneTimeEvent())
            }.onFailure {
                _userFollowers.postValue(ViewState.Error(it.message ?: it.localizedMessage).toOneTimeEvent())
            }
        }
    }

    private val _userFollowing = MutableLiveData<OneTimeEvent<ViewState<List<UserDomain>>>>()
    val userFollowing: LiveData<OneTimeEvent<ViewState<List<UserDomain>>>>
        get() = _userFollowing

    fun getInfoFollowing(){
        viewModelScope.launch {
            _userFollowing.postValue(ViewState.Loading.toOneTimeEvent())
            repository.userFollowing(username).onSuccess {
                _userFollowing.postValue(ViewState.DataLoaded(it).toOneTimeEvent())
            }.onFailure {
                _userFollowing.postValue(ViewState.Error(it.message ?: it.localizedMessage).toOneTimeEvent())
            }
        }
    }

    fun addUserToFavorite(user: UserDomain){
        viewModelScope.launch {
            repository.setUserFavorite(user)
        }
    }

    fun removeUserFromFavorite(userId: Int){
        viewModelScope.launch {
            repository.removeUserFavorite(userId)
        }
    }

    suspend fun isUserFavorite(userId: Int): LiveData<Result<Boolean>> = repository.isUserFavorite(userId).asLiveData(coroutineContext)

}
