package com.f1k375.gitex.data.repositories

import com.f1k375.gitex.data.local.LocalClient
import com.f1k375.gitex.data.local.asEntity
import com.f1k375.gitex.data.remote.UserPersistence
import com.f1k375.gitex.domain.UserDomain
import com.f1k375.gitex.domain.asDomain
import kotlinx.coroutines.flow.*

class UserRepositoryImpl(
    private val persistence: UserPersistence,
    private val local: LocalClient
): UserRepository {
    override suspend fun searchUser(key: String): Result<List<UserDomain>> {
        val result = persistence.searchUser(key)
        return result.map { it.asSequence().map { data-> data.asDomain }.toList() }
    }

    override suspend fun detailUser(username: String): Result<UserDomain> {
        val result = persistence.detailUser(username)
        return result.map { it.asDomain }
    }

    override suspend fun userFollowers(username: String): Result<List<UserDomain>> {
        val result = persistence.userFollowers(username)
        return result.map { it.asSequence().map {data-> data.asDomain }.toList() }
    }

    override suspend fun userFollowing(username: String): Result<List<UserDomain>> {
        val result = persistence.userFollowing(username)
        return result.map { it.asSequence().map {data-> data.asDomain }.toList() }
    }

    override fun favoriteUser(): Flow<Result<List<UserDomain>>> =
        local.userDao.getFavorites().map {list-> list.asSequence().map { it.asDomain }.toList() }.map { Result.success(it) }.catch { error-> Result.failure<List<UserDomain>>(error) }

    override fun isUserFavorite(userId: Int): Flow<Result<Boolean>> =
        local.userDao.isUserExist(userId).map {user-> if (user == null) Result.success(false) else Result.success(true) }.catch {error->
            emit(Result.failure(error))
        }

    override suspend fun setUserFavorite(user: UserDomain) {
        local.userDao.add(user.asEntity)
    }

    override suspend fun removeUserFavorite(userId: Int) {
        local.userDao.removeUser(userId)
    }
}
