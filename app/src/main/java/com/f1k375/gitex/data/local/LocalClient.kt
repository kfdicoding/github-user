package com.f1k375.gitex.data.local

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [UserEntity::class],
    version = 1,
    exportSchema = false
)
abstract class LocalClient: RoomDatabase(){
    abstract val userDao: UserDao
}
