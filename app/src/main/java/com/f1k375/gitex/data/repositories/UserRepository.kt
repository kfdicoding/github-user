package com.f1k375.gitex.data.repositories

import com.f1k375.gitex.domain.UserDomain
import kotlinx.coroutines.flow.Flow

interface UserRepository {

    suspend fun searchUser(key: String): Result<List<UserDomain>>
    suspend fun detailUser(username: String): Result<UserDomain>
    suspend fun userFollowers(username: String): Result<List<UserDomain>>
    suspend fun userFollowing(username: String): Result<List<UserDomain>>

    fun favoriteUser(): Flow<Result<List<UserDomain>>>
    fun isUserFavorite(userId: Int): Flow<Result<Boolean>>
    suspend fun setUserFavorite(user: UserDomain)
    suspend fun removeUserFavorite(userId: Int)


}
