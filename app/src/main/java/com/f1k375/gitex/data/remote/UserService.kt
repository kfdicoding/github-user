package com.f1k375.gitex.data.remote

import com.f1k375.gitex.data.models.SearchResponse
import com.f1k375.gitex.data.models.UserResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface UserService {

    @GET("search/users")
    suspend fun searchUser(
        @Query("q") key: String
    ): Response<SearchResponse>

    @GET("users/{username}")
    suspend fun detailUser(
        @Path("username") username: String
    ): Response<UserResponse>

    @GET("users/{username}/followers")
    suspend fun userFollowers(
        @Path("username") username: String
    ): Response<List<UserResponse>>

    @GET("users/{username}/following")
    suspend fun userFollowing(
        @Path("username") username: String
    ): Response<List<UserResponse>>


}
