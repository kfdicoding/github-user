package com.f1k375.gitex.data.remote

import com.f1k375.gitex.data.models.UserResponse

interface UserPersistence {

    suspend fun searchUser(key: String): Result<List<UserResponse>>
    suspend fun detailUser(username: String): Result<UserResponse>
    suspend fun userFollowers(username: String): Result<List<UserResponse>>
    suspend fun userFollowing(username: String): Result<List<UserResponse>>

}
