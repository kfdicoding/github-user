package com.f1k375.gitex.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.f1k375.gitex.domain.UserDomain

@Entity(tableName = "users")
data class UserEntity(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val loginName: String,
    val htmlUrl: String,
    val avatar: String
)

val UserDomain.asEntity
get() = UserEntity(
    id,
    loginName,
    htmlUrl,
    avatarUrl
)
