package com.f1k375.gitex.data.models

import com.google.gson.annotations.SerializedName

data class SearchResponse(
    @SerializedName("total_count")
    val total: Int? = null,

    @SerializedName("incomplete_results")
    val isError: Boolean = false,

    @SerializedName("items")
    val items: List<UserResponse>? = null
)
