package com.f1k375.gitex.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(user: UserEntity)

    @Query("SELECT * FROM users")
    fun getFavorites(): Flow<List<UserEntity>>

    @Query("SELECT * FROM users WHERE id=:id")
    fun isUserExist(id:Int): Flow<UserEntity?>

    @Query("DELETE FROM users WHERE id=:id")
    suspend fun removeUser(id: Int)

}
