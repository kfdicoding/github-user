package com.f1k375.gitex.data.models

import com.google.gson.annotations.SerializedName

data class UserResponse(
	@SerializedName("login")
	val login: String? = null,

	@SerializedName("bio")
	val bio: String? = null,

	@SerializedName("html_url")
	val htmlUrl: String? = null,

	@SerializedName("created_at")
	val createdAt: String? = null,

	@SerializedName("type")
	val type: String? = null,

	@SerializedName("updated_at")
	val updatedAt: String? = null,

	@SerializedName("company")
	val company: String? = null,

	@SerializedName("id")
	val id: Int? = null,

	@SerializedName("public_repos")
	val publicRepos: Int? = null,

	@SerializedName("email")
	val email: String? = null,

	@SerializedName("followers")
	val followers: Int? = null,

	@SerializedName("avatar_url")
	val avatarUrl: String? = null,

	@SerializedName("following")
	val following: Int? = null,

	@SerializedName("name")
	val name: String? = null,

	@SerializedName("location")
	val location: String? = null,
)
