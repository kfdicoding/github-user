package com.f1k375.gitex.data.remote

import com.f1k375.gitex.data.models.UserResponse
import com.google.gson.JsonNull
import com.google.gson.JsonParser
import java.io.IOException

class UserPersistenceImpl(
    private val service: UserService
): UserPersistence {
    private suspend fun <T : Any> safeApiCall(
        call: suspend () -> Result<T>,
        errorMessage: String
    ): Result<T> {
        return try {
            call()
        } catch (e: Exception) {
            e.printStackTrace()
            Result.failure(exception = IOException(errorMessage + " : " + e.message, e))
        }
    }

    private fun errorMessage(response: String?): String {
        val fileJsonResponse = try {
            JsonParser.parseString(response)
        } catch (e: Exception) {
            JsonNull.INSTANCE
        }
        return if (fileJsonResponse.asJsonObject.has("message"))
            fileJsonResponse.asJsonObject.get("message").asString
        else "Unknown"
    }

    override suspend fun searchUser(key: String): Result<List<UserResponse>> = safeApiCall(
        call = {
            val response = service.searchUser(key)
            if (response.isSuccessful) Result.success(response.body()?.items ?: emptyList())
            else Result.failure(
                Exception("code ${response.code()} : ${errorMessage(response.errorBody()?.string())}")
            )
        },
        errorMessage = "Failed to perform search\nCrash on system"
    )

    override suspend fun detailUser(username: String): Result<UserResponse> = safeApiCall(
        call = {
            val response = service.detailUser(username)
            if (response.isSuccessful) {
                val data = response.body() ?: UserResponse()
                Result.success(data)
            }
            else Result.failure(
                Exception("code ${response.code()} : ${errorMessage(response.errorBody()?.string())}")
            )
        },
        errorMessage = "Failure to get detail\nCrash on system"
    )

    override suspend fun userFollowers(username: String): Result<List<UserResponse>> = safeApiCall(
        call = {
            val response = service.userFollowers(username)
            if (response.isSuccessful) Result.success(response.body() ?: emptyList())
            else Result.failure(
                Exception("code ${response.code()} : ${errorMessage(response.errorBody()?.string())}")
            )
        },
        errorMessage = "Failed to get followers\nCrash on System"
    )

    override suspend fun userFollowing(username: String): Result<List<UserResponse>> = safeApiCall(
        call = {
            val response = service.userFollowing(username)
            if (response.isSuccessful) Result.success(response.body() ?: emptyList())
            else Result.failure(
                Exception("code ${response.code()} : ${errorMessage(response.errorBody()?.string())}")
            )
        },
        errorMessage = "Failed to get following\nCrash on System"
    )
}
